<?php
// Je déclare mes variables en haut de page
$prenom = 'Christophe';
$age = 27;
?>
<!DOCTYPE html>
<html>
<head>
    <title>O'Clock - Initiation PHP - Exemple 2</title>
</head>
<body>
    <p>Bonjour à tous ! Je m'appelle <?php echo $prenom; ?> et j'ai <?php echo $age; ?> ans.</p>
</body>
</html>