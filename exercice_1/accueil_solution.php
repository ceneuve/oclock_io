<?php

## Ajouter ici la condition sur les données envoyées

if (empty($_POST['prenom'])){

    header('Location: index.php');

} else if (empty($_POST['nom'])){

    header('Location: index.php');
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>O'Clock - Introduction - Facebook</title>
    <link rel="stylesheet" type="text/css" href="assets/accueil.css">
</head>
<body>
    <div id="headbar">
        <div id="headbarContent">
            <div class="menu_top">
                <img src="assets/top.png" />
            </div>
            <a href="" class="menu_dyn">
                <img src="assets/sheldon.jpg" id="avatar">
                <span id="name">
                    <?php echo $_POST['prenom'] . ' ' . $_POST['nom']; ?>
                </span>
            </a>
            <div class="menu_top">
                <img src="assets/topbis.png" />
            </div>
        </div>
    </div>
</body>
</html>