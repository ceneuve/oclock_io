<!DOCTYPE html>
<html>
<head>
    <title>O'Clock - Introduction - Facebook</title>
    <link rel="stylesheet" type="text/css" href="assets/connection.css">
</head>
<body>
    <div id="headbar">
        <div id="menubar">
            <h1>
                <a href="" title="Accéder à la page d’accueil">
                    <img src="assets/facebook_logo.png" />
                </a>
            </h1>
            <form id="login_form" action="accueil.php" method="post">
                <table cellspacing="0">
                    <tbody>
                        <tr>
                            <td class="html7magic">
                                <label for="email">Prénom</label>
                            </td>
                            <td class="html7magic">
                                <label for="pass">Nom</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input class="inputtext" name="prenom" tabindex="1" type="text">
                            </td>
                            <td>
                                <input class="inputtext" name="nom" tabindex="2" type="text">
                            </td>
                            <td>
                                <label class="uiButton uiButtonConfirm" id="loginbutton" for="u_0_r">
                                    <input value="Connexion" tabindex="4" type="submit">
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td class="login_form_label_field"></td>
                            <td class="login_form_label_field">
                                <div>
                                    <a href="#" id="forgot_account_link">
                                        Informations de compte oubliées&nbsp;?
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
    <div id="globalContainer">
        <div id="content">
            <div id="block_left">
                <div id="txt_left">Avec Facebook, partagez et restez en contact avec votre entourage.</div>
                <img class="img" src="assets/reseau.png" alt="" width="537" height="195">
            </div>
            <div id="block_right">
                <div id="txt_right">
                    <div id="msg_inscription">Inscription</div>
                    <div id="msg_ssinscription">C’est gratuit (et ça le restera toujours)</div>
                </div>
                <form>

                </form>
            </div>
        </div>
    </div>
    <div id="pageFooter">
        Des liens un copyright &copy; etc.
    </div>
</body>
</html>